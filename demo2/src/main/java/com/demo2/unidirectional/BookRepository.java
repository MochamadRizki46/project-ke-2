package com.demo2.unidirectional;

import org.springframework.data.domain.Page;  
import org.springframework.data.domain.Pageable;  
import org.springframework.data.jpa.repository.JpaRepository;  
import org.springframework.data.jpa.repository.Modifying;  
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import com.demo2.model.Book;

public interface BookRepository extends JpaRepository<Book, Integer>{  
}